/** Created by Nicholas Hillier on 2015-04-12. */
var sparkwood = angular.module('sparkwood', ['ngSanitize', 'ngResource', 'ngCookies', 'ngRoute']);

(function(){
    'use strict';

    sparkwood.config(sparkwoodConfig);
    sparkwoodConfig.$inject = ['$routeProvider', '$locationProvider'];

    function sparkwoodConfig($routeProvider, $locationProvider) {

        $routeProvider.when('/', {
            templateUrl: 'templates/home.html',
            controller: 'HomeController'
        });

        $routeProvider.otherwise({redirectTo: '/'});
        $locationProvider.html5Mode(true);
    }
})();
