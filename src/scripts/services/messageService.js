/** Created by Nicholas Hillier on 2015-04-12. */
(function(){
    'use strict';

    sparkwood.factory('messageService', messageService);
    messageService.$inject = [];

    function messageService() {

        return {
            getMsg: getMsg
        };

        function getMsg(){
            return "Welcome to Sparkwood.";
        }
    }

})();